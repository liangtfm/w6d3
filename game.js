(function (root) {


  var Asteroids = root.Asteroids = (root.Asteroids || {});

  var Game = Asteroids.Game = function(ctx, numAst) {
    this.ctx = ctx;
    this.asteroids = [];

    this.addAsteroids(numAst);

    this.bullets = [];

    this.ship = new Asteroids.Ship([250, 250], [0, 0], 10, "green");
  };

  Game.DIM_X = 500;
  Game.DIM_Y = 500;

  Game.prototype.addAsteroids = function(numAsteroids) {
    for (var i = 0; i < numAsteroids; i++) {
      this.asteroids = this.asteroids.concat(Asteroids.Asteroid.randomAsteroid(Game.DIM_X, Game.DIM_Y));
    }
  };

  Game.prototype.draw = function(ctx) {
    ctx.clearRect(0, 0, Game.DIM_X, Game.DIM_Y);

    this.ship.draw(ctx);

    this.bullets.forEach(function(bullet) {
      bullet.draw(ctx);
    });

    this.asteroids.forEach(function(asteroid) {
      asteroid.draw(ctx);
    });
  };

  Game.prototype.move = function() {
    this.ship.move();

    this.bullets.forEach(function(bullet) {
      bullet.move();
    });

    this.asteroids.forEach(function(asteroid) {
      asteroid.move();
    });
  };

  Game.prototype.step = function(ctx) {
    this.move();
    this.draw(ctx);
  };

  Game.prototype.checkCollisions = function(id) {

    var ship = this.ship;

    this.asteroids.forEach(function(asteroid) {

      if (asteroid.isCollidedWith(ship)) {
        window.clearInterval(id);
        alert("YOU LOSE!");
      }

    });

    return false;
  };

  Game.prototype.outOfBounds = function() {
    var goodAsteroids = [];

    this.asteroids.forEach(function(asteroid) {
      if ((asteroid.pos[0] < 500 && asteroid.pos[0] > 0) && (asteroid.pos[1] < 500 && asteroid.pos[1] > 0)) {
        goodAsteroids = goodAsteroids.concat(asteroid);
      }
    });

    this.asteroids = goodAsteroids;
  }

  Game.prototype.fireBullet = function () {
    var bullet = this.ship.fireBullet();

    this.bullets = this.bullets.concat(bullet);

    console.log(this.bullets);
  };


  // FINISH THESE METHODS
  Game.prototype.removeAsteroid(asteroid) {

  };

  Game.prototype.removeBullet(bullet) {

  };

  Game.prototype.bindKeyHandlers = function () {
    var ship = this.ship;
    var game = this;

    key('up', function(){
      ship.power([0, -1]);
    });

    key('down', function() {
      ship.power([0, 1]);
    });

    key('left', function() {
      ship.power([-1, 0]);
    });

    key('right', function() {
      ship.power([1, 0]);
    });

    key('a', function() {
      game.fireBullet();
    });
  };

  Game.prototype.start = function(ctx) {
    var game = this;
    game.bindKeyHandlers();
    // game.step(ctx);
    // game.step(ctx);

    var id = window.setInterval(function() {
      game.step(ctx);
      game.checkCollisions(id);
      game.outOfBounds();
    }, 30);
  };

})(this);