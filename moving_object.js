(function (root) {
  var Asteroids = root.Asteroids = (root.Asteroids || {});

  var MovingObject = Asteroids.MovingObject = function (pos, vel, radius, color) {
    this.pos = pos;
    this.vel = vel;
    this.radius = radius;
    this.color = color;
  };

  MovingObject.prototype.move = function() {

    var dx = this.vel[0];
    var dy = this.vel[1];

    this.pos[0] = this.pos[0] + dx;
    this.pos[1] = this.pos[1] + dy;
  };

  MovingObject.prototype.draw = function(ctx) {
    ctx.fillStyle = this.color;
    ctx.beginPath();

    var x = this.pos[0];
    var y = this.pos[1];

    ctx.arc(
      x,
      y,
      this.radius,
      0,
      2 * Math.PI,
      false
    );

    ctx.fill();
  };

  MovingObject.prototype.isCollidedWith = function(otherObject) {
    var x1 = this.pos[0];
    var x2 = otherObject.pos[0];
    var y1 = this.pos[1];
    var y2 = otherObject.pos[1];

    var distance = Math.sqrt(
      Math.pow((x2-x1),2) + Math.pow((y2-y1),2)
    );

    var radSum = this.radius + otherObject.radius;

    return (radSum > distance);
  };

})(this); // (this) will make it execute immedaitely