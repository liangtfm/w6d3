(function(root) {
  Function.prototype.inherits = function (Parent) {
    function Surrogate() {};
    Surrogate.prototype = Parent.prototype;
    this.prototype = new Surrogate();
  }
  var Asteroids = root.Asteroids = (root.Asteroids || {} );

  var Ship = Asteroids.Ship = function(pos, vel, radius, color) {
    Asteroids.MovingObject.call(this, [250, 250], [0, 0], Ship.RADIUS, Ship.COLOR)
  };

  Ship.inherits(Asteroids.MovingObject);

  Ship.RADIUS = 10;
  Ship.COLOR = "Green";

  Ship.prototype.power = function(impulse) {
    var dx = impulse[0];
    var dy = impulse[1];

    this.vel[0] += dx;
    this.vel[1] += dy;

  };

  // FIGURE THIS OUT
  // Need to pass bullet game instance or class?
  Ship.prototype.fireBullet = function() {
    var vx = this.vel[0];
    var vy = this.vel[1];

    if (this.vel === [0, 0]) {
      return;
    } else {
      var speed = Math.sqrt(Math.pow(vx, 2) + Math.pow(vy, 2));
      var dx = (vx / speed);
      var dy = (vy / speed);

      var X = this.pos[0];
      var Y = this.pos[1];

      return new Asteroids.Bullet([X, Y], [dx * 20, dy * 20], Asteroids.Bullet.RADIUS, Asteroids.Bullet.COLOR);
    }
  };

})(this);