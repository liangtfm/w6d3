(function (root) {

  Function.prototype.inherits = function (Parent) {
    function Surrogate() {};
    Surrogate.prototype = Parent.prototype;
    this.prototype = new Surrogate();
  }

  var Asteroids = root.Asteroids = (root.Asteroids || {});

  var Bullet = Asteroids.Bullet = function(pos, vel, radius, color) {
    Asteroids.MovingObject.call(this, pos, vel, Bullet.RADIUS, Bullet.COLOR);

    this.game = Asteroids.Game;
  };

  Bullet.inherits(Asteroids.MovingObject);

  Bullet.RADIUS = 3;
  Bullet.COLOR = "black";

  // FINISH THIS METHOD
  Bullet.prototype.hitAsteroids = function() {
    var astArray = Asteroids.Game.asteroids;
    var bullet = this;

    astArray.forEach(function(asteroid) {
      if (asteroid.isCollidedWith(bullet)) {
        bullet.game.removeAsteroid(asteroid);
        bullet.game.removeBullet(bullet);
      };
    });
  }

})(this);