(function (root) {
  Function.prototype.inherits = function (Parent) {
    function Surrogate() {};
    Surrogate.prototype = Parent.prototype;
    this.prototype = new Surrogate();
  };

  var Asteroids = root.Asteroids = (root.Asteroids || {});

  var Asteroid = Asteroids.Asteroid = function (pos, vel, radius, color) {
    Asteroids.MovingObject.call(this, pos, vel, Asteroid.RADIUS, Asteroid.COLOR);
  };

  Asteroid.inherits(Asteroids.MovingObject); //prototype inheritance

  Asteroid.COLOR = "red";
  Asteroid.RADIUS = 15;

  Asteroid.randomAsteroid = function(dimX, dimY) {
    // below is wrapped up in function: returns new constructor
    var x = (dimX * Math.random());
    var y = (dimY * Math.random());
    var randomPos = [x, y];

    // 4 * Math.random();
    var dx = ((Math.random() * 10) - 1);
    var dy = ((Math.random() * 10) - 1);
    var randomVel = [dx, dy];

    return new Asteroid(randomPos, randomVel, Asteroid.RADIUS, Asteroid.COLOR);
  };


})(this);